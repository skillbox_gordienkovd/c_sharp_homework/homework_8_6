#nullable enable
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text.Json;
using homework_8_6.company;
using homework_8_6.menu;

namespace homework_8_6.system
{
    public class ProgramSystem
    {
        private const string DbFile = "db.json";
        private Organization Organization { get; set; } = new();

        public void StartSystem()
        {
            if (CheckDbExists())
                LoadDb();
            else
                Organization = new Organization();

            var menuIndex = 0;

            while (true)
            {
                var organizationMenuChoice = ShowOrganizationMenu(ref menuIndex);

                if (organizationMenuChoice != null)
                    switch (organizationMenuChoice)
                    {
                        case OrganizationMenu.EditName:
                            ShowOrganizationEditName();
                            break;
                        case OrganizationMenu.Departments:
                            while (true)
                            {
                                var departmentsMenuChoice = ShowDepartmentsMenu(ref menuIndex);

                                if (departmentsMenuChoice != null)
                                    switch (departmentsMenuChoice)
                                    {
                                        case DepartmentsMenu.AddDepartment:
                                            Organization.Departments ??= new List<Department>();
                                            var newDepartment = ShowAddDepartment(Organization.DepartmentSequence);
                                            if (newDepartment != null)
                                            {
                                                Organization.Departments.Add(newDepartment);
                                                Organization.DepartmentSequence++;
                                                SaveDb();
                                            }

                                            break;
                                        case DepartmentsMenu.ShowDepartments:
                                            Organization.Departments ??= new List<Department>();
                                            var choice = ShowDepartmentsList(Organization.Departments, ref menuIndex);
                                            if (choice == null)
                                            {
                                                break;
                                            }
                                            else
                                            {
                                                while (true)
                                                {
                                                    var departmentMenuChoice =
                                                        ShowDepartmentMenu(choice, ref menuIndex);
                                                    if (departmentMenuChoice != null)
                                                        switch (departmentMenuChoice)
                                                        {
                                                            case DepartmentMenu.EditName:
                                                                ShowDepartmentEditName(choice);
                                                                break;
                                                            case DepartmentMenu.AddDepartment:
                                                                var addedDepartment =
                                                                    ShowAddDepartment(Organization.DepartmentSequence);
                                                                if (addedDepartment != null)
                                                                {
                                                                    choice.Departments ??= new List<Department>();
                                                                    choice.Departments.Add(addedDepartment);
                                                                    Organization.DepartmentSequence++;
                                                                    SaveDb();
                                                                    break;
                                                                }
                                                                else
                                                                {
                                                                    break;
                                                                }
                                                            case DepartmentMenu.ShowDepartments:
                                                                var choice2 = ShowDepartmentsList(choice.Departments,
                                                                    ref menuIndex);
                                                                if (choice2 == null)
                                                                    break;
                                                                else
                                                                    break;
                                                            case DepartmentMenu.AddEmployee:
                                                                var newEmployee =
                                                                    ShowAddEmployee(Organization.EmployeeSequence,
                                                                        choice.Id);
                                                                if (newEmployee == null)
                                                                {
                                                                    break;
                                                                }
                                                                else
                                                                {
                                                                    choice.Employees ??= new List<Employee>();
                                                                    choice.Employees.Add(newEmployee);
                                                                    Organization.EmployeeSequence++;
                                                                    SaveDb();
                                                                    break;
                                                                }
                                                            case DepartmentMenu.ShowEmployees:
                                                                var employeeChoice =
                                                                    ShowEmployeesList(choice.Employees, ref menuIndex);
                                                                if (employeeChoice == null)
                                                                {
                                                                }
                                                                else
                                                                {
                                                                    while (true)
                                                                    {
                                                                        var employeeMenuChoice =
                                                                            ShowEmployeeMenu(employeeChoice,
                                                                                ref menuIndex);

                                                                        if (employeeMenuChoice == null)
                                                                            break;
                                                                        switch (employeeMenuChoice)
                                                                        {
                                                                            case EmployeeMenu.EditLastname:
                                                                                ShowEmployeeEditLastName(
                                                                                    employeeChoice);
                                                                                break;
                                                                            case EmployeeMenu.EditFirstname:
                                                                                ShowEmployeeEditFirstName(
                                                                                    employeeChoice);
                                                                                break;
                                                                            case EmployeeMenu.EditAge:
                                                                                ShowEmployeeEditAge(employeeChoice);
                                                                                break;
                                                                            case EmployeeMenu.EditSalary:
                                                                                ShowEmployeeEditSalary(employeeChoice);
                                                                                break;
                                                                        }
                                                                    }
                                                                }

                                                                break;
                                                        }
                                                    else
                                                        break;
                                                }

                                                break;
                                            }
                                    }
                                else
                                    break;
                            }

                            break;
                    }
            }
        }

        private OrganizationMenu? ShowOrganizationMenu(ref int menuIndex)
        {
            Console.CursorVisible = false;
            var menuItems = Enum.GetValues<OrganizationMenu>();

            while (true)
            {
                Console.WriteLine("Управление в меню: Подтвердить - Enter, Назад - Backspace");
                Console.WriteLine($"Организация: {Organization.Name}");
                for (var i = 0; i < menuItems.Length; i++)
                {
                    var attributes = (DescriptionAttribute[]) menuItems[i]
                        .GetType()
                        .GetField(menuItems[i].ToString())
                        ?.GetCustomAttributes(typeof(DescriptionAttribute), false);

                    if (i == menuIndex)
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine(attributes?[0].Description!);
                    }
                    else
                    {
                        Console.WriteLine(attributes?[0].Description!);
                    }

                    Console.ResetColor();
                }

                var ckey = Console.ReadKey();

                switch (ckey.Key)
                {
                    case ConsoleKey.DownArrow when menuIndex == menuItems.Length - 1:
                        menuIndex = 0;
                        break;
                    case ConsoleKey.DownArrow:
                        menuIndex++;
                        break;
                    case ConsoleKey.UpArrow when menuIndex == 0:
                        menuIndex = menuItems.Length - 1;
                        break;
                    case ConsoleKey.UpArrow:
                        menuIndex--;
                        break;
                    case ConsoleKey.Enter:
                    {
                        Console.Clear();
                        var choice = menuItems[menuIndex];
                        menuIndex = 0;
                        return choice;
                    }
                }

                Console.Clear();
                return null;
            }
        }

        private void ShowOrganizationEditName()
        {
            Console.WriteLine("Введите наименование организации:");

            while (true)
            {
                var result = Console.ReadLine();
                if (result != "" || result.Trim() != "")
                {
                    Organization.Name = result;
                    SaveDb();
                    Console.Clear();
                    break;
                }

                var ckey = Console.ReadKey();

                if (ckey.Key == ConsoleKey.Backspace)
                {
                    Console.Clear();
                    break;
                }
            }
        }

        private DepartmentsMenu? ShowDepartmentsMenu(ref int menuIndex)
        {
            var menuItems = Enum.GetValues<DepartmentsMenu>();

            while (true)
            {
                for (var i = 0; i < menuItems.Length; i++)
                {
                    var attributes = (DescriptionAttribute[]) menuItems[i]
                        .GetType()
                        .GetField(menuItems[i].ToString())
                        ?.GetCustomAttributes(typeof(DescriptionAttribute), false);

                    if (i == menuIndex)
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine(attributes?[0].Description!);
                    }
                    else
                    {
                        Console.WriteLine(attributes?[0].Description!);
                    }

                    Console.ResetColor();
                }

                var ckey = Console.ReadKey();

                switch (ckey.Key)
                {
                    case ConsoleKey.DownArrow when menuIndex == menuItems.Length - 1:
                        menuIndex = 0;
                        break;
                    case ConsoleKey.DownArrow:
                        menuIndex++;
                        break;
                    case ConsoleKey.UpArrow when menuIndex == 0:
                        menuIndex = menuItems.Length - 1;
                        break;
                    case ConsoleKey.UpArrow:
                        menuIndex--;
                        break;
                    case ConsoleKey.Backspace:
                        menuIndex = 0;
                        Console.Clear();
                        return null;
                    case ConsoleKey.Enter:
                    {
                        Console.Clear();
                        var choice = menuItems[menuIndex];
                        menuIndex = 0;
                        return choice;
                    }
                }

                Console.Clear();
            }
        }

        private Department? ShowAddDepartment(long newId)
        {
            var department = new Department();

            Console.WriteLine("Введите наименование департамента");

            while (true)
            {
                var result = Console.ReadLine();
                if (result != "" || result.Trim() != "")
                {
                    department.Id = newId;
                    department.Name = result;
                    department.Created = DateTime.Now;
                    department.Modified = DateTime.Now;
                    Console.Clear();
                    break;
                }

                var ckey = Console.ReadKey();

                if (ckey.Key == ConsoleKey.Backspace)
                {
                    Console.Clear();
                    return null;
                }
            }

            return department;
        }

        private Department? ShowDepartmentsList(List<Department> departments, ref int menuIndex)
        {
            while (true)
            {
                for (var i = 0; i < departments.Count; i++)
                {
                    if (i == menuIndex)
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine(departments[i].ToString());
                    }
                    else
                    {
                        Console.WriteLine(departments[i].ToString());
                    }

                    Console.ResetColor();
                }

                var ckey = Console.ReadKey();

                switch (ckey.Key)
                {
                    case ConsoleKey.DownArrow when menuIndex == departments.Count - 1:
                        menuIndex = 0;
                        break;
                    case ConsoleKey.DownArrow:
                        menuIndex++;
                        break;
                    case ConsoleKey.UpArrow when menuIndex == 0:
                        menuIndex = departments.Count - 1;
                        break;
                    case ConsoleKey.UpArrow:
                        menuIndex--;
                        break;
                    case ConsoleKey.Backspace:
                        menuIndex = 0;
                        Console.Clear();
                        return null;
                    case ConsoleKey.Enter:
                    {
                        Console.Clear();
                        var choice = departments[menuIndex];
                        menuIndex = 0;
                        return choice;
                    }
                }

                Console.Clear();
            }
        }

        private DepartmentMenu? ShowDepartmentMenu(Department department, ref int menuIndex)
        {
            var menuItems = Enum.GetValues<DepartmentMenu>();

            while (true)
            {
                Console.WriteLine($"Департамент: {department.Name}");

                for (var i = 0; i < menuItems.Length; i++)
                {
                    var attributes = (DescriptionAttribute[]) menuItems[i]
                        .GetType()
                        .GetField(menuItems[i].ToString())
                        ?.GetCustomAttributes(typeof(DescriptionAttribute), false)!;

                    if (i == menuIndex)
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine(attributes?[0].Description!);
                    }
                    else
                    {
                        Console.WriteLine(attributes?[0].Description!);
                    }

                    Console.ResetColor();
                }

                var ckey = Console.ReadKey();

                switch (ckey.Key)
                {
                    case ConsoleKey.DownArrow when menuIndex == menuItems.Length - 1:
                        menuIndex = 0;
                        break;
                    case ConsoleKey.DownArrow:
                        menuIndex++;
                        break;
                    case ConsoleKey.UpArrow when menuIndex == 0:
                        menuIndex = menuItems.Length - 1;
                        break;
                    case ConsoleKey.UpArrow:
                        menuIndex--;
                        break;
                    case ConsoleKey.Backspace:
                        menuIndex = 0;
                        Console.Clear();
                        return null;
                    case ConsoleKey.Enter:
                    {
                        Console.Clear();
                        var choice = menuItems[menuIndex];
                        menuIndex = 0;
                        return choice;
                    }
                }

                Console.Clear();
            }
        }

        private void ShowDepartmentEditName(Department department)
        {
            Console.WriteLine("Введите наименование департамента:");

            while (true)
            {
                var result = Console.ReadLine();
                if (result != "" || result.Trim() != "")
                {
                    department.Name = result;
                    department.Modified = DateTime.Now;
                    SaveDb();
                    Console.Clear();
                    break;
                }

                var ckey = Console.ReadKey();

                if (ckey.Key == ConsoleKey.Backspace)
                {
                    Console.Clear();
                    break;
                }
            }
        }

        private Employee? ShowAddEmployee(long newId, long departmentId)
        {
            var employee = new Employee {Id = newId, DepartmentId = departmentId};

            while (true)
            {
                Console.WriteLine("Введите фамилию:");

                var result = Console.ReadLine();
                if (result != "" || result.Trim() != "")
                {
                    employee.Lastname = result;
                    Console.Clear();
                    break;
                }

                var ckey = Console.ReadKey();

                if (ckey.Key == ConsoleKey.Backspace)
                {
                    Console.Clear();
                    return null;
                }
            }

            while (true)
            {
                Console.WriteLine("Введите имя: ");

                var result = Console.ReadLine();
                if (result != "" || result.Trim() != "")
                {
                    employee.Firstname = result;
                    Console.Clear();
                    break;
                }

                var ckey = Console.ReadKey();

                if (ckey.Key == ConsoleKey.Backspace)
                {
                    Console.Clear();
                    return null;
                }
            }

            while (true)
            {
                Console.WriteLine("Введите возраст: ");

                var result = int.TryParse(Console.ReadLine(), out var age);
                if (result && age is > 18 and < 110)
                {
                    employee.Age = age;
                    Console.Clear();
                    break;
                }

                var ckey = Console.ReadKey();

                if (ckey.Key == ConsoleKey.Backspace)
                {
                    Console.Clear();
                    return null;
                }
            }

            while (true)
            {
                Console.WriteLine("Введите зарплату: ");

                var result = double.TryParse(Console.ReadLine(), out var salary);
                if (result && salary > 50000)
                {
                    employee.Salary = salary;
                    Console.Clear();
                    break;
                }

                var ckey = Console.ReadKey();

                if (ckey.Key == ConsoleKey.Backspace)
                {
                    Console.Clear();
                    return null;
                }
            }

            return employee;
        }

        private Employee? ShowEmployeesList(List<Employee> employees, ref int menuIndex)
        {
            while (true)
            {
                for (var i = 0; i < employees.Count; i++)
                {
                    if (i == menuIndex)
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine(employees[i].ToString());
                    }
                    else
                    {
                        Console.WriteLine(employees[i].ToString());
                    }

                    Console.ResetColor();
                }

                var ckey = Console.ReadKey();

                switch (ckey.Key)
                {
                    case ConsoleKey.DownArrow when menuIndex == employees.Count - 1:
                        menuIndex = 0;
                        break;
                    case ConsoleKey.DownArrow:
                        menuIndex++;
                        break;
                    case ConsoleKey.UpArrow when menuIndex == 0:
                        menuIndex = employees.Count - 1;
                        break;
                    case ConsoleKey.UpArrow:
                        menuIndex--;
                        break;
                    case ConsoleKey.Backspace:
                        menuIndex = 0;
                        Console.Clear();
                        return null;
                    case ConsoleKey.Enter:
                    {
                        Console.Clear();
                        var choice = employees[menuIndex];
                        menuIndex = 0;
                        return choice;
                    }
                }

                Console.Clear();
            }
        }

        private EmployeeMenu? ShowEmployeeMenu(Employee employee, ref int menuIndex)
        {
            var menuItems = Enum.GetValues<EmployeeMenu>();

            while (true)
            {
                Console.WriteLine(employee.ToString());

                for (var i = 0; i < menuItems.Length; i++)
                {
                    var attributes = (DescriptionAttribute[]) menuItems[i]
                        .GetType()
                        .GetField(menuItems[i].ToString())
                        ?.GetCustomAttributes(typeof(DescriptionAttribute), false)!;

                    if (i == menuIndex)
                    {
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.WriteLine(attributes?[0].Description!);
                    }
                    else
                    {
                        Console.WriteLine(attributes?[0].Description!);
                    }

                    Console.ResetColor();
                }

                var ckey = Console.ReadKey();

                switch (ckey.Key)
                {
                    case ConsoleKey.DownArrow when menuIndex == menuItems.Length - 1:
                        menuIndex = 0;
                        break;
                    case ConsoleKey.DownArrow:
                        menuIndex++;
                        break;
                    case ConsoleKey.UpArrow when menuIndex == 0:
                        menuIndex = menuItems.Length - 1;
                        break;
                    case ConsoleKey.UpArrow:
                        menuIndex--;
                        break;
                    case ConsoleKey.Backspace:
                        menuIndex = 0;
                        Console.Clear();
                        return null;
                    case ConsoleKey.Enter:
                    {
                        Console.Clear();
                        var choice = menuItems[menuIndex];
                        menuIndex = 0;
                        return choice;
                    }
                }

                Console.Clear();
            }
        }

        private void ShowEmployeeEditLastName(Employee employee)
        {
            Console.WriteLine("Введите фамилию:");

            while (true)
            {
                var result = Console.ReadLine();
                if (result != "" || result.Trim() != "")
                {
                    employee.Lastname = result;
                    SaveDb();
                    Console.Clear();
                    break;
                }

                var ckey = Console.ReadKey();

                if (ckey.Key == ConsoleKey.Backspace)
                {
                    Console.Clear();
                    break;
                }
            }
        }

        private void ShowEmployeeEditFirstName(Employee employee)
        {
            Console.WriteLine("Введите имя:");

            while (true)
            {
                var result = Console.ReadLine();
                if (result != "" || result.Trim() != "")
                {
                    employee.Firstname = result;
                    SaveDb();
                    Console.Clear();
                    break;
                }

                var ckey = Console.ReadKey();

                if (ckey.Key == ConsoleKey.Backspace)
                {
                    Console.Clear();
                    break;
                }
            }
        }

        private void ShowEmployeeEditAge(Employee employee)
        {
            Console.WriteLine("Введите возраст:");

            while (true)
            {
                var result = int.TryParse(Console.ReadLine(), out var age);
                if (result && age is > 18 and < 110)
                {
                    employee.Age = age;
                    SaveDb();
                    Console.Clear();
                    break;
                }

                var ckey = Console.ReadKey();

                if (ckey.Key == ConsoleKey.Backspace)
                {
                    Console.Clear();
                    break;
                }
            }
        }

        private void ShowEmployeeEditSalary(Employee employee)
        {
            Console.WriteLine("Введите зарплату:");

            while (true)
            {
                var result = int.TryParse(Console.ReadLine(), out var salary);
                if (result && salary > 50000)
                {
                    employee.Salary = salary;
                    SaveDb();
                    Console.Clear();
                    break;
                }

                var ckey = Console.ReadKey();

                if (ckey.Key == ConsoleKey.Backspace)
                {
                    Console.Clear();
                    break;
                }
            }
        }

        private static bool CheckDbExists()
        {
            var fileInfo = new FileInfo(DbFile);
            return fileInfo.Exists;
        }

        private void LoadDb()
        {
            Organization = JsonSerializer.Deserialize<Organization>(File.ReadAllText(DbFile));
        }

        private void SaveDb()
        {
            File.WriteAllText(DbFile, JsonSerializer.Serialize(Organization));
        }
    }
}