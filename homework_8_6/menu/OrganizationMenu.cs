using System.ComponentModel;

namespace homework_8_6.menu
{
    public enum OrganizationMenu
    {
        [Description("Изменить наименование организации")]
        EditName,
        [Description("Департаменты")]
        Departments
    }
}