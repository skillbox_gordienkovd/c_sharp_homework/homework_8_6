using System.ComponentModel;

namespace homework_8_6.menu
{
    public enum DepartmentMenu
    {
        [Description("Изменить наименование департамента")]
        EditName,
        [Description("Добавить департамент")]
        AddDepartment,
        [Description("Показать список департаментов")]
        ShowDepartments,
        [Description("Добавить сотрудника")]
        AddEmployee,
        [Description("Показать список сотрудников департамента")]
        ShowEmployees
    }
}