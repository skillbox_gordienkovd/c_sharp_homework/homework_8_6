using System.ComponentModel;

namespace homework_8_6.menu
{
    public enum EmployeeMenu
    {
        [Description("Изменить фамилию")]
        EditLastname,
        [Description("Изменить имя")]
        EditFirstname,
        [Description("Изменить возраст")]
        EditAge,
        [Description("Изменить зарплату")]
        EditSalary
    }
}