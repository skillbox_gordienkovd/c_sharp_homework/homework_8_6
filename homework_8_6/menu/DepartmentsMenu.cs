using System.ComponentModel;

namespace homework_8_6.menu
{
    public enum DepartmentsMenu
    {
        [Description("Показать список департаментов")]
        ShowDepartments,
        [Description("Добавить департамент")]
        AddDepartment
    }
}