using System.Collections.Generic;

namespace homework_8_6.company
{
    public class Organization
    {
        public string Name { get; set; }
        public long DepartmentSequence { get; set; } = 1;
        public long EmployeeSequence { get; set; } = 1;
        public List<Department> Departments { get; set; } = new();
    }
}