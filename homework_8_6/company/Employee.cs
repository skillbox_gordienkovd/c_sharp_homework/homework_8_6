namespace homework_8_6.company
{
    public class Employee
    {
        public long Id { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public int Age { get; set; }
        public long DepartmentId { get; set; }
        public double Salary { get; set; }

        public override string ToString()
        {
            return
                $"{nameof(Id)}: {Id}, Фамилия: {Lastname}, Имя: {Firstname}, Возраст: {Age}, Id департамента: {DepartmentId}, Зарплата: {Salary}";
        }
    }
}