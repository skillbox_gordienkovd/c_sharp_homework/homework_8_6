using System;
using System.Collections.Generic;

namespace homework_8_6.company
{
    public class Department
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public List<Department> Departments { get; set; } = new();
        public List<Employee> Employees { get; set; } = new();

        public override string ToString()
        {
            return
                $"{nameof(Id)}: {Id}, Наименование: {Name}, Дата создания: {Created}, Дата последнего изменения: {Modified}";
        }
    }
}