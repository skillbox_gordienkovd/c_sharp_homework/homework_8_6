﻿using System;
using homework_8_6.system;

namespace homework_8_6
{
    class Program
    {
        static void Main(string[] args)
        {
            var programSystem = new ProgramSystem();
            programSystem.StartSystem();
        }
    }
}